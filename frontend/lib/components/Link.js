import React, { Component } from 'react';

import NextLink from 'next/link';

export default class Link extends Component {

  render() {

    const { children, href, ...props } = this.props;

    return (
      <NextLink href={ href }>
        <a href={ href } { ...props } rel={ this.rel() }>
          { children }
        </a>
      </NextLink>
    );

  }

  rel() {

    const { href = '', rel = ''  } = this.props;

    const rels = rel.split(/\s+/)
      .filter(r => r.length)
      .reduce((o, r) => ({
        ...o,
        [r]: true,
      }), {
        noreferrer: !!href.match(/^(https?:)?\/\//i),
      });

    return Object.keys(rels).filter(r => rels[r]).sort().join(' ') || undefined;

  }

}
