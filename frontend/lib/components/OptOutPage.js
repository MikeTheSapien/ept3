import React, { Component } from 'react';

import css from 'next/css';

import api     from '../api';
import Wrapper from './Wrapper';

const style = {
  error: css({ maxWidth: '30em', color: 'red' }),
};

export default class OptOutPage extends Component {

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      submitting: false,
      email:      props.initialEmail || '',
      error:      null,
      success:    null,
    };
  }

  render() {
    const { lang } = this.props;
    const { error, success, submitting } = this.state;

    const title = lang === 'es'
      ? 'Desuscribirse'
      : 'Opt Out / Unsubscribe';

    return (
      <Wrapper { ...this.props } title={ title }>
        {
          lang === 'es' ? (
            <p>
              Si ha recibido un email de esta página web que no ha pedido, le pedimos disculpas. Lamentablemente a veces hay gente horrible que abusa de servicios gratuitos como este usando la dirección de email de otras&#160;personas.
            </p>
          ) : (
            <p>
              Apologies if you received an email from this website that you didn't request. Unfortunately sometimes awful people like to abuse free services like this by entering the email addresses of&#160;others.
            </p>
          )
        }
        {
          lang === 'es' ? (
            <p>
              Escribiendo su dirección de email debajo hará que no reciba ningún otro email de esta página&#160;web.
            </p>
          ) : (
            <p>
              Submitting your email address below will mean you wont receive any further email from this&#160;website.
            </p>  
          )
        }
        <form
          disabled = { submitting }
          ref      = { el => this._form = el }
          onSubmit = { this.onSubmit }
        >
          <input
            type        = "email"
            name        = "email"
            placeholder = { lang === 'es' ? 'Dirección de correo' : 'Email Address' }
            onChange    = { this.onChange }
            value       = { this.state.email }
            readOnly    = { submitting }
            autoFocus   = { true }
          />
          <button disabled = { submitting }>
            { lang === 'es' ? 'Desuscribirse' : 'Opt Out' }
          </button>
        </form>
        {
          success && (
            <p>
              <strong>Opted out { success }</strong>
            </p>
          )
        }
        {
          error && (
            <p className={ style.error }>
              { error }
            </p>
          )
        }
      </Wrapper>
    );

  }

  onChange(e) {

    const email = e.currentTarget.value;
    if (email !== this.state.email) {
      this.setState({
        email,
        error:   null,
        success: null,
      });
    }

  }

  onSubmit(e) {

    e.preventDefault();

    const { submitting, email } = this.state;

    if (submitting) return;
    if (email.indexOf('@') === -1) return;

    this.setState({
      submitting: true,
      success:    null,
      error:      null,
    }, async () => {

      try {
        await api.post('optout', { email });
        this.setState({
          submitting: false,
          email:      '',
          success:    email,
        });

      } catch (err) {

        this.setState({
          error:      err.message,
          submitting: false,
        });

      }

    });

  }

}
