import React, { Component } from 'react';

import Wrapper from './Wrapper';

export default class TestDescriptionPage extends Component {

  render() {

    const { name, desc, lang } = this.props;

    return (
      <Wrapper { ...this.props } title = { `Test: ${name}` }>

        <p style={{ whiteSpace: 'pre-wrap' }}>
          { desc }
        </p>

      </Wrapper>
    );

  }

}
