import React, { Component } from 'react';

import css from 'next/css';

const style = {
  root: css({
    margin: '0.3em 1em 0 1em',
  }),
  list: css({
    listStyleType: 'none',
    margin:        0,
    padding:       0,
  }),
};

export default class TestResultsAgents extends Component {

  render() {

    const agents = this.agents();
    if (!agents.length) return null;

    return (
      <div className={ style.root }>
        <h3>User Agents</h3>
        <ul className={ style.list }>
          {
            agents.map(agent => (
              <li key={ agent }>
                { agent }
              </li>
            ))
          }
        </ul>
      </div>
    );

  }

  agents() {

    const { results } = this.props;
    const agents = {};
    results.forEach(({ http = {} }) => {
      if (http.httpUserAgent) {
        agents[http.httpUserAgent] = true;
      }
    });
    return Object.keys(agents).sort();

  }

}
