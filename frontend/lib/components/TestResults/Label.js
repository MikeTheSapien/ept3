import React, { Component } from 'react';

import Link from '../Link';
import ServerDate from '../../ServerDate';

import moment from 'moment';
import css    from 'next/css';

const style = {
  root: css({
    flexGrow:        1,
    maxWidth:        '16em',
    display:         'flex',
    alignItems:      'center',
    justifyContent:  'space-around',
    flexDirection:   'column',
    padding:         '0.5em 1em',
    margin:          '0.2em',
    borderRadius:    '0.5em',
    minHeight:       '3.1em',
    fontWeight:      'bold',
    textDecoration:  'none',
  }),
  unHit: css({
    backgroundColor: '#ccc',
    color:           '#333',
    ':hover':        { backgroundColor: '#bbb' },
  }),
  hit: css({
    backgroundColor: '#c00',
    color:           'white',
    transition:      'background-color 1s linear',
  }),
  flash: css({
    backgroundColor: '#cc0',
    transition:      'background-color 0s linear',
  }),
  age: css({
    fontSize:  '0.9em',
    fontStyle: 'italic',
  }),
};

export default class TestResultsLabel extends Component {

  constructor() {
    super();
    this.state = {
      age:   null,
      flash: false,
    };
    this.refreshAge = this.refreshAge.bind(this);
  }

  componentDidMount() { this.refreshAge(); }

  componentDidUpdate(prevProps, prevState) {

    if (!prevState.flash && this.state.flash) {
      clearTimeout(this.clearFlashPid);
      this.clearFlashPid = setTimeout(() => {
        if (this.state.flash) this.setState({ flash: false });
      }, 1);
    }

    this.refreshAge();
  }

  componentWillUnmount() {
    clearTimeout(this.refreshAgePid);
    clearTimeout(this.clearFlashPid);
  }

  componentWillReceiveProps({ results }) {
    if (!this.state.flash && results.length > this.props.results.length) {
      clearTimeout(this.clearFlashPid);
      this.setState({ flash: true });
    }
  }

  render() {

    const { id, name, lang, results } = this.props;

    const styles = [ style.root ];
    styles.push(style[results.length ? 'hit' : 'unHit']);
    if (this.state.flash) styles.push(style.flash);

    let href = `/testDescription?test=${id}`;
    if (lang !== 'en') href = `/${lang}${href}`;

    return (
      <Link
        href      = { href }
        className = { styles.join(' ') }
      >
        { name }
        {
          results.length ? (
            <span className={ style.age }>
              { this.state.age }
            </span>
          ) : null
        }
      </Link>
    );

  }

  refreshAge() {

    const { results } = this.props;

    let age = null;
    let ageSecs = 0;
    if (results.length) {
      let { time } = results[results.length - 1];
      const serverDate = new ServerDate();
      if (serverDate.now() < time) time = serverDate.now();
      const dateNow = serverDate.date();
      ageSecs = parseInt((dateNow.getTime() - time) / 1000, 10);
      age = ageSecs < 1 ? 'Just now'
        : ageSecs === 1 ? '1 second ago'
        : ageSecs < 60  ? `${ageSecs} seconds ago`
        : moment(time).from(dateNow);
    }

    clearTimeout(this.refreshAgePid);
    delete this.refreshAgePid;

    if (age !== this.state.age) {
      this.setState({ age });
    }

    if (results.length) {
      this.refreshAgePid = setTimeout(this.refreshAge, ageSecs < 60 ? 1000 : 10000);
    }

  }

}
