import React, { Component } from 'react';

import Label from './Label';

import css from 'next/css';

const style = {
  root: css({
    display:   'flex',
    flexWrap:  'wrap',
    maxWidth:  '60em',
    marginTop: '1em',
  }),
};

export default class TestResults extends Component {

  render() {
    const { lang } = this.props;

    return (
      <div className={ style.root }>
        {
          this.tests().map(test => (
            <Label lang={ lang } key={ test.id } { ...test }/>
          ))
        }
      </div>
    );

  }

  /**
   * Returns a sorted array of tests
   */
  tests() {

    const tests = this.testsWithResults();

    return Object.keys(tests).sort((a, b) => {

      const ar = tests[a].results;
      const br = tests[b].results;

      const aa = ar.length ? ar[ar.length - 1].time * -1 : 0;
      const bb = br.length ? br[br.length - 1].time * -1 : 0;

      if (aa === 0 && bb !== 0) return bb * -1;
      if (bb === 0 && aa !== 0) return aa;
      if (aa !== 0 && bb !== 0) return aa < bb ? aa : bb * -1;

      const an = tests[a].name.toLowerCase();
      const bn = tests[b].name.toLowerCase();
      return an < bn ? -1 : an > bn ? 1 : 0;

    }).map(id => ({
      id,
      ...tests[id],
    }));

  }

  /**
   * Adds the results into the tests
   */
  testsWithResults() {

    const { tests } = this.props;
    const results = this.results();

    return Object.keys(tests).reduce((o, id) => ({
      ...o,
      [id]: {
        ...tests[id],
        results: results[id] || [],
      }
    }), {});

  }

  /**
   * Converts an array of results into a handy object, keyed on the test id
   */
  results() {

    const { results } = this.props;
    return results.reduce((o, res) => ({
      ...o,
      [res.test]: res.test in o ? [ ...o[res.test], res ] : [ res ],
    }), {});

  }

}
