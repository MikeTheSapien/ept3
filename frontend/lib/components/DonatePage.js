import React, { Component } from 'react';

import Wrapper from './Wrapper';

import Link from 'next/link';

export default class DonatePage extends Component {

  render() {

    const { lang } = this.props;

    const title = lang === 'es'
        ? 'Donaciones'
        : 'Donations / Contributions';

    return (
      <Wrapper { ...this.props } title={ title }>
        <div>
          <p>
            {
                lang === 'es'
                    ? 'Si ha encontrado útil este sitio y desea entregar algo a cambio o contribuir al hosting y al tiempo de desarrollo, tengo '
                    : 'If you found this site useful and wish to give something back or contribute to the hosting and development time, I have '
            }
            <Link href="bitcoin:1PQLtWnjUi1itHLG6QCQeHM3Nxua8pRsq1">Bitcoin</Link>, <Link href="xmr:44zg8QFZza5cMBWcEF4VqsU5panam7tMhfB6fFdzWbDpBcaVy7feqQXKD92smbYGy3C7KPA6d7Q6UWY8f11noJLWUrVKJAK">Monero</Link>, <Link href="zcash:zcAN9ij9YZ7Cc9HMqCfcjpusELTTKQWutgjSRH9Dxx5E4DLTKTxRye5FgFs7vDPn1edSoBNFvLPTxn7byjgwQaMVJeJ64kQ">Z-Cash</Link>
            { lang === 'es' ? ' y ' : ' and ' }
            <Link href="https://www.paypal.me/grepular">Paypal</Link>.
          </p>
          {
            lang === 'es' ? (
              <p>
                Si desea contribuir con algo de su tiempo y es un desarrollador, todo el código ha
                sido <Link href="https://gitlab.com/mikecardwell/ept3">lanzado</Link> bajo GPL-3.0
                y estoy encantado de aceptar contribuciones.
              </p>
            ) : (
              <p>
                If you wish to contribute some of your time and you're a developer,
                all of the code has been <Link href="https://gitlab.com/mikecardwell/ept3">released</Link> under
                GPL-3.0 and I'm happy to accept contributions.
              </p>
            )
          }
        </div>
      </Wrapper>
    );
  }

}
