import React, { Component } from 'react';
import NoScript from 'react-noscript';

import css  from 'next/css';
import Link from 'next/link';

import api     from '../api';
import Wrapper from './Wrapper';

const style = {
  error:        css({ maxWidth: '30em', color: 'red' }),
  instructions: css({ maxWidth: '30em' }),
  form:         css({ marginTop: '1em' }),
};

export default class HomePage extends Component {

  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      submitting: false,
      email:      props.initialEmail,
      error:      null,
      success:    null,
    };
  }

  render() {
    const { lang } = this.props;
    const { error, success, submitting } = this.state;

    return (
      <Wrapper { ...this.props } title="Email Privacy Tester">
        <p className={ style.instructions }>
          {
            lang === 'es' ? (
              <span>
                Si esta es su primera vez aquí, puede querer leer
                la <Link href="/es/about">página sobre nosotros</Link> y/o
                la <Link href="/es/privacy">política de privacidad</Link>.
              </span>
            ) : (
              <span>
                If this is your first time here, you might want to read
                the <Link href="/about">about page</Link> and/or
                the <Link href="/privacy">privacy policy</Link>.
              </span>
            )
          }
          <NoScript>
            <span style={{ color: 'red' }}>
              <br/>
              Sorry, but you'll need to enable JavaScript to use the form
              below. On the plus side, there is no cross-origin content.
            </span>
          </NoScript>
        </p>
        <form
          className = { style.form }
          disabled  = { submitting }
          ref       = { el => this._form = el }
          onSubmit  = { this.onSubmit }
        >
          <input type="hidden" name="lang" value={ lang }/>
          <input
            type        = "email"
            name        = "email"
            placeholder = { lang === 'es' ? 'Correo electrónico' : 'Email Address' }
            onChange    = { this.onChange }
            value       = { this.state.email }
            readOnly    = { submitting }
            autoFocus   = { true }
          />
          <button disabled = { submitting }>
            {
              submitting ? (
                lang === 'es' ? 'Enviando' : 'Sending'
              ) : (
                lang === 'es' ? 'Enviar' : 'Submit'
              )
            }
          </button>
        </form>
        {
          success && (
            lang == 'es' ? (
              <p className={ style.instructions }>
                <strong>Email enviado con éxito a { success.email }</strong>.
                Cuando haya llegado, clique el enlace de confirmación y será
                llevado a una página desde donde podrá activar los emails de
                prueba que serán enviados a su correo electrónico.
              </p>
            ) : (
              <p className={ style.instructions }>
                <strong>Email successfully sent to { success.email }</strong>.
                Once it has arrived, click the confirm link, and you will be
                taken to a page where you can trigger test emails to be sent
                to your email address.
              </p>
            )
          )
        }
        {
          error && (
            <p className={ style.error }>
              { error }
            </p>
          )
        }
      </Wrapper>
    );

  }

  onChange(e) {

    const email = e.currentTarget.value;
    if (email !== this.state.email) {
      this.setState({
        email,
        error:   null,
        success: null,
      });
    }

  }

  onSubmit(e) {

    e.preventDefault();

    const { lang } = this.props;
    const { submitting, email } = this.state;

    if (submitting) return;
    if (email.indexOf('@') === -1) return;

    this.setState({
      submitting: true,
      success:    null,
      error:      null,
    }, async () => {

      try {

        this.setState({
          submitting: false,
          email:      '',
          success: {
            email,
            response: await api.post('sendConfirm', { email, lang }),
          },
        });

      } catch (err) {

        this.setState({
          error:      err.message,
          submitting: false,
        });

      }

    });

  }

}
