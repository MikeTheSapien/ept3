import React, { Component } from 'react';

import Link from '../Link';
import css  from 'next/css';

const style = {
  root: css({
    display:         'flex',
    flexDirection:   'column',
    justifyContent:  'center',
    alignItems:      'center',
    backgroundColor: '#333',
    color:           'white',
    minHeight:       '10vh',
    padding:         '1em',
    marginTop:       '1em',
    fontWeight:      'bold',
  }),
  topLine: css({
    margin: '1em 0',
  }),
  link: css({
    color:   'white',
  }),
  imgLink: css({
    display: 'inline-block',
    padding: '0.5em',
  }),
};

export default class Footer extends Component {

  render() {
    const { lang } = this.props;

    return (
      <footer className={ style.root }>

        <div className={ style.topLine }>
          {
            lang === 'es' ? (
              'Diseño de la web, producción y hosting por'
            ) : (
              'Website design, production and hosting by'
            )
          }
          &#160;
          <Link href="https://grepular.com" className={ style.link }>
            Mike Cardwell
          </Link>
        </div>

        <div>
          <Link
            href  = "https://www.digitalocean.com/?refcode=2529a7adf896"
            title = "This Website, hosted on Digital Ocean - Fast, Cheap, Reliable VPS hosting (affiliate link)"
            className = { style.imgLink }
          >
            <img alt="Digital Ocean" src="/static/images/digitalocean-badge-white.png" width="140"/>
          </Link>
          <Link
            href  = "bitcoin:1PQLtWnjUi1itHLG6QCQeHM3Nxua8pRsq1"
            title = "Leave a Bitcoin tip"
            className = { style.imgLink }
          >
            <img alt="Bitcoin" src="/static/images/bitcoin.png"/>
          </Link>
          <Link
            href  = "xmr:44zg8QFZza5cMBWcEF4VqsU5panam7tMhfB6fFdzWbDpBcaVy7feqQXKD92smbYGy3C7KPA6d7Q6UWY8f11noJLWUrVKJAK"
            title = "Leave a Monero tip"
            className = { style.imgLink }
          >
            <img alt="Monero" src="/static/images/monero.svg" width="26" height="26"/>
          </Link>
          <Link
            href  = "zcash:zcAN9ij9YZ7Cc9HMqCfcjpusELTTKQWutgjSRH9Dxx5E4DLTKTxRye5FgFs7vDPn1edSoBNFvLPTxn7byjgwQaMVJeJ64kQ"
            title = "Leave a Zcash tip"
            className = { style.imgLink }
          >
            <img alt="Zcash" src="/static/images/zcash.png"/>
          </Link>
          <Link
            href  = "https://www.paypal.me/grepular"
            title = "Leave a Paypal tip"
            className = { style.imgLink }
          >
            <img alt="Paypal" src="/static/images/paypal.png"/>
          </Link>
          <Link
            href  = "//twitter.com/mickeyc"
            title = "Twitter"
            className = { style.imgLink }
          >
            <img alt="Twitter" src="/static/images/twitter-26x26.png"/>
          </Link>
        </div>
      </footer>
    );

  }

}
