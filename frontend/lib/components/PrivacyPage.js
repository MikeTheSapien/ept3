import React, { Component } from 'react';

import Wrapper from './Wrapper';

export default class PrivacyPage extends Component {

  render() {

    const { lang } = this.props;

    const title = lang === 'es'
        ? 'Política de Privacidad'
        : 'Privacy Policy';

    return (
      <Wrapper { ...this.props } title={ title }>
        <div>
          {
            lang === 'es' ? (
              <p>
                Esta app requiere su dirección de correo electrónico para funcionar.
                Una vez la escriba, la almacena en una base de datos antes de ser
                borrada. Esto es para poder limitar y reaccionar al posible abuso.
                La dirección de email aparecerá también en mis registros de correo,
                que datan de diez días. No compartiré su dirección de correo con
                nadie, nunca. Jamás enviaré ningún email más allá del email
                referente a esta aplicación cuando usted lo solicite.
                El email que le envío contiene un enlace de salida. Si lo clica,
                almacenaré la dirección de email en la base de datos durante lo
                que dure el sistema y esta app no podrá contactarlo de nuevo.
              </p>
            ) : (
              <p>
                This app requires your email address in order to work. Once you
                submit it, it is kept in a database for one week
                before being deleted. This is so I can apply rate limiting and react
                to abuse. The email address will also appear in my mail server logs,
                which go back as far as ten days. I will not share your email
                address with anyone, ever. I will never send any email to it other
                than the email that this application is designed to send
                out when you submit it. The email that I do send to you contains an
                opt-out link. If you click it, I will store your email address in
                the database for the lifetime of this system and this app will be
                prevented from contacting it again.
              </p>
            )
          }
          {
            lang === 'es' ? (
              <p>
                También almaceno resultados de test durante una semana antes de
                borrarlos. Si no guardase los resultados, no se los podría mostrar.
                Esto puede implicar el almacenamiento de los siguientes datos:
              </p>
            ) : (
              <p>
                I also store test results for a period of one week before deletion.
                If I didn't store the results, I could not display them to you. This
                may consist of any of the following data:
              </p>
            )
          }
          <ol>
            {
              (lang === 'es' ? [
                'Las fechas y horas a las que abrió el email que le envié',
                'Las direcciones IP desde las que su cliente de correo se conecta',
                'Las direcciones IP de los servidores DNS que usa su cliente de correo',
                'Los Agentes-Usuario HTTP que envían petición de header enviados por su cliente de correo',
                'Las peticiones de header X-Forwarded-For HTTP añadidas por sus proxies web',
                'Los nombres de los test que su correo falló',
              ] : [
                'The dates and times that you opened the email that I sent you',
                'The IP addresses that your email clients connect from',
                'The IP addresses of the DNS servers your email clients use',
                'The User-Agent HTTP request headers sent by your email clients',
                'The X-Forwarded-For HTTP request headers added by your web proxies',
                'The names of the tests which your email clients failed',
              ]).map((item, n) => <li key={ n }>{ item }</li>)
            }
          </ol>
          {
            lang === 'es' ? (
              <p>
                También tengo registros de servidor web estilo Apache para administrar
                el sitio, analizar tendencias y combatir abusos. 
              </p>
            ) : (
              <p>
                I also have standard Apache-style web server logs, for administering
                the site, analysing trends and combatting abuse.
              </p>
            )
          }
          {
            lang === 'es' ? (
              <p>
                Este sitio no realiza ninguna petición cross-origin a terceros de los
                que no se pueda fiar. Este sitio no usa cookies de ningún tipo.
                Respeto su derecho a la privacidad e intento ser lo más abierto
                posible sobre cómo uso sus datos. Si tiene preguntas, por
                favor <a href="https://grepular.com">contácteme</a>.
              </p>
            ) : (
              <p>
                This site does not perform any automated cross-origin requests to
                third parties which you may not trust. This site doesn't use
                cookies, of any type. I respect your right to privacy and
                try to be as open as possible about how I use your data. If you have
                any questions, please <a href="https://grepular.com">contact me</a>.
              </p>
            )
          }
          {
            lang === 'es' ? (
              <p>
                El código fuente completo de esta aplicación está disponible
                en: <a href="https://gitlab.com/mikecardwell/ept3">
                  https://gitlab.com/mikecardwell/ept3
                </a>
              </p>
            ) : (
              <p>
                The full source code for this application is available
                at <a href="https://gitlab.com/mikecardwell/ept3">
                  https://gitlab.com/mikecardwell/ept3
                </a>
              </p>
            )
          }
        </div>
      </Wrapper>
    );
  }

}
