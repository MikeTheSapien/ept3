export const backendUrl         = 'http://localhost:5001';
export const frontendUrl        = 'http://localhost:5000';
export const frontendDomain     = 'localhost';
export const backendUrlInternal = 'http://ept_backend:3000';
