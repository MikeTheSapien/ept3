import React, { Component } from 'react';

import DonatePage from '../lib/components/DonatePage';

export default class Donate extends Component {

  render() {
    return (
      <DonatePage lang="en" { ...this.props }/>
    );
  }

}
