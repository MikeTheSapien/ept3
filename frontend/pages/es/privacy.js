import React, { Component } from 'react';

import PrivacyPage from '../../lib/components/PrivacyPage';

export default class Privacy extends Component {

  render() {
    return (
      <PrivacyPage lang="es" { ...this.props }/>
    );
  }

}