import React, { Component } from 'react';

import api        from '../../lib/api';
import OptOutPage from '../../lib/components/OptOutPage';

export default class OptOut extends Component {

  static async getInitialProps ({ query }) {

    const { email, code } = query;

    let initialEmail = email || '';

    if (!initialEmail && code) {

      try {

        const test = await api.get('test', { code, lang: 'es' });
        initialEmail = test.email;

      } catch (err) {

        // Probably expired

      }

    }

    return { initialEmail };
  }

  render() {
    return <OptOutPage lang="es" { ...this.props }/>;
  }

}
