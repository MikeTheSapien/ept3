import React, { Component } from 'react';

import TestPage from '../../lib/components/TestPage';

import api from '../../lib/api';

export default class Test extends Component {

  static async getInitialProps({ query }) {
    const lang = 'es';
    
    const { code } = query;

    const testList    = api.get('testList', { lang });
    const testResults = api.get('testResults', { code });

    let email;
    let tests;
    let initialResults;
    let testSent = false;
    try {

      const test     = await api.get('test', { code });
      email          = test.email;
      testSent       = !!test.testEmailsSent;
      tests          = await testList;
      initialResults = await testResults;
    } catch(err) {
      // Probably expired
    }

    return { code, email, tests, initialResults, testSent };
  }

  render() {
    return (
      <TestPage lang="es" { ...this.props }/>
    );
  }
}
