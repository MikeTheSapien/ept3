import Email from '../lib/Email';

import Tests from '../lib/db/model/Tests';

import * as confirmEmail from '../emails/confirm';

/**
 * Rate limit helper function
 */
async function rateLimit(ip, email, age, limit) {

  const count = (
    await Tests.aggregate([
      {
        $project: {
          email: { $toLower: '$email' },
          ip:    '$submit.ip.client',
          time:  '$submit.time',
        },
      },
      {
        $match: {
          time: { $gt: new Date(Date.now() - age) },
          $or: [
            { ip },
            { email: email.toLowerCase().trim() },
          ],
          time: { $gt: new Date(Date.now() - age) },
        },
      },
    ])
  ).length;

  if (count >= limit) {

    throw new Error('You have been rate limited. Please try again later');

  }

}

export default async function sendConfirmRoute (req, res) {

  const { email = '', lang = 'en' } = req.body;

  try {

    const ip = req.remote();

    /**
     * Apply rate limiting
     */
    await Promise.all([
      rateLimit(ip.client, email,  1800000, 10),
      rateLimit(ip.client, email,  3600000, 15),
      rateLimit(ip.client, email, 43200000, 20),
      rateLimit(ip.client, email, 86400000, 25),
    ]);

    if (email.match(/\.ru$/i)) {
      return res.json({ error: "What Russia is doing to Ukraine is sickening and monstrous. I'll consider serving Russian email addresses when the entire of Ukraine is free and Russia has paid for the unprovoked murder, rape, genocide and kidnapping of civilians" });
    }

    /**
     * Save to DB
     */
    const test = new Tests({
      email,
      submit: { ip },
    });
    await test.save();

    /**
     * Send confirmation email
     */
    const response = await Email.send({
      ...confirmEmail[lang],
      to: email,
    }, {
      code:     test._id,
      ip:       test.submit.ip.client,
      email:    email,
    });

    res.json({ ok: response });

  } catch (err) {

    return res.json({ error: err.message });

  }

}
