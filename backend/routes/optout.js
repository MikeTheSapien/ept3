import Optouts from '../lib/db/model/Optouts';

export default async function codeToEmailRoute (req, res) {

  const email = (req.body.email || '').trim().toLowerCase();

  try {

    if (await Optouts.find({ email }).count() === 0) {

      const optout = new Optouts({
        email: email,
        ip:    req.remote(),
      });
      await optout.save();

    }

    res.json({ ok: true });

  } catch (err) {

    return res.json({ error: err.message });

  }


}
