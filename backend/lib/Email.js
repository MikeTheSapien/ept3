import React from 'react';
import { renderToStaticMarkup } from 'react-dom/server';

import nodeMailer    from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';
import Optouts       from './db/model/Optouts';

import * as config from '../config';

const {
  mailServerHost = 'localhost',
  mailServerPort = 25,
  mailServerTls  = false,
  mailServerTlsOnConnect = false,
} = config;

const transporter = nodeMailer.createTransport(smtpTransport({
  host:      mailServerHost,
  port:      mailServerPort,
  ignoreTLS: !mailServerTls,
  secure:    mailServerTlsOnConnect,
}));

function replaceTemplateParams (msg, params) {

  if (!params) return msg;

  if (Array.isArray(msg)) {
    return msg.map(m => replaceTemplateParams(m, params));
  }

  if (typeof msg === 'function') {
    // Assume React Component
    return renderToStaticMarkup(React.createElement(msg, params));
  }

  if (typeof msg === 'object' && msg !== null) {
    return Object.keys(msg).reduce((o, k) => ({
      ...o,
      [k]: replaceTemplateParams(msg[k], params),
    }), {});
  }

  if (typeof msg === 'string') {
    return Object.keys(params).reduce((txt, param) => {
      const k = param.split(/([A-Z])/).map(s => s.replace(/^([A-Z])$/, `_${s}`)).join('');
      const rx = new RegExp(`%%${k.toUpperCase()}%%`, 'g');
      return txt.replace(rx, params[param]);
    }, msg);
  }

  return msg;

}

export default {

  async send (options, params = {}) {

    const msg = replaceTemplateParams(options, {
      ...config,
      ...params,
    });

    const optout = await Optouts.find({
      email: options.to.toLowerCase().trim(),
    }).limit(1).count();
    if (optout) {
      throw new Error(`${options.to} has opted out of receiving email from this system`);
    }

    const { accepted, response } = await transporter.sendMail(msg);
    if (accepted.length === 0) throw new Error(response);
    return response;

  },

};
