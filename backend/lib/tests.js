export const applet = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta applet' : 'Applet tag',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<applet codebase="http://TRACKING_URL/" code="applet.class"></applet>
`});

export const atom = (lang='en') => ({
  name: lang === 'es' ? 'Feed Atom' : 'Atom feed',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <head> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <head> of the HTML part, place a tag as follows:'
  )) + `

<link rel="alternate" type="application/atom+xml" href="http://TRACKING_URL/">
`});

export const audio = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta audio' : 'Audio tag',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<audio src="http://TRACKING_URL/" preload="metadata" autoplay="autoplay"></audio>
`});

export const background = (lang='en') => ({
  name: lang === 'es' ? 'Atributo de fondo' : 'Background attribute',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga un atributo como el siguiente:'
  ) : (
    'In the <body> of the HTML part, place an attribute as follows:'
  )) + `

<body background="http://TRACKING_URL/">
`});

export const backgroundImage = (lang='en') => ({
  name: lang === 'es' ? 'Imagen de fondo CSS' : 'CSS background-image',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<p style="background-image:url('http://TRACKING_URL/');"></p>
`});

export const bgsound = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta Bgsound' : 'BGSound tag',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<bgsound src="http://TRACKING_URL/" loop="1">
`});

export const css = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta Enlace CSS' : 'CSS link tag',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <head> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <head> of the HTML part, place a tag as follows:'
  )) + `

<link rel="stylesheet" type="text/css" href="http://TRACKING_URL/">
`});

export const cssAttachment = (lang='en') => ({
  name: lang === 'es' ? 'Adjunto CSS' : 'CSS Attachment',
  type: 'http',
  desc: lang === 'es' ? `
En el <body> de la parte HTML, ponga una etiqueta como la siguiente:

<link rel="stylesheet" type="text/css" href="cid:ATTACHMENT_ID">

Después añada una multiparte/texto relacionado/archivo css al email con el encabezado MIME:

Content-ID:<ATTACHMENT_ID>

Dentro del texto especialmente creado/archivo css, añada un estilo CSS que realice una petición http. Específicamente:

background-image:url('http://TRACKING_URL/');
` : `
In the <body> of the HTML part, place a tag as follows:

<link rel="stylesheet" type="text/css" href="cid:ATTACHMENT_ID">

Then attach a multipart/related text/css file to the email with the MIME header:

Content-ID:<ATTACHMENT_ID>

Inside that specially crafted text/css file, add a CSS style which performs a http request. Specifically:

background-image:url('http://TRACKING_URL/');
`
});

export const cssBehavior = (lang='en') => ({
  name: lang === 'es' ? 'Conducta CSS' : 'CSS behavior',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<p style="behavior:url('http://TRACKING_URL/') url('http://TRACKING_URL/')"></p>
`});

export const cssContent = (lang='en') => ({
  name: lang === 'es' ? 'Contenido CSS' : 'CSS content',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<p style="content:url('http://TRACKING_URL/')"></p>
`});

export const cssEscape = (lang='en') => ({
  name: lang === 'es' ? 'Escape CSS' : 'CSS escape',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML ponga lo siguiente:'
  ) : (
    'In the <body> of the HTML part place the following:'
  )) + `

<div id="escapeCss"></div>
<style type="text/css">
  #escapeCss {
    background: \\75 \\72 \\6C ('http://TRACKING_URL/');
  }
</style>
`});

export const cssImport = (lang='en') => ({
  name: lang === 'es' ? 'Importar CSS' : 'CSS import',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<style type="text/css">@import 'http://TRACKING_URL/';</style>
`});

export const dispositionNotification = (lang='en') => ({
  name: lang === 'es' ? 'Notificación de Disposición' : 'Disposition Notification',
  type: 'email',
  desc: lang === 'es' ? `
Añada un encabezado al mensaje como el siguiente:

Disposition-Notification: <TRACKING_ADDRESS@example.com>
  
Las notificaciones de envío y las notificaciones de lectura, más importantes, podrán ser enviadas a esta dirección:
` : `
Add a message header as follows to the email message:

Disposition-Notification: <TRACKING_ADDRESS@example.com>

Delivery notifications and more importantly read notifications may then be sent to this address
`});

export const dnsAnchor = (lang='en') => ({
  name: 'DNS Prefetch - Anchor',
  type: 'dns',
  desc: lang === 'es' ? `
En el <body> de la parte HTML, ponga una etiqueta como la siguiente:

<a href="http://TRACKING_URL/"></a>

Algunos clientes de correo electrónico y navegadores web realizan pre-fetching de DNS en URLs dentro de etiquets Anchor de emails. Este test no filtrará su dirección de IP, pero filtrará la dirección IP de sus resolvers DNS, que pueden proveer información sobre qué proveedor de Internet está usando y potencialmente sobre su localización.

Algunos servidores de correo hacen búsquedas de DNS en estas URL como parte de su filtrado de spam, así que puede que vea la dirección IP de los servidores resolver de DNS aquí, además de la suya.
` : `
In the <body> of the HTML part, place a tag as follows:

<a href="http://TRACKING_URL/"></a>

Some email clients and web browsers perform DNS pre-fetching on URLs in Anchor tags of emails. This test wont leak your IP address, but it will leak the IP address of your DNS resolvers, which can provide information on which ISP you're using and potentially your general location.

Some mail servers do DNS lookups on these URLs as part of their spam filtering process, so you may see the IP address of the mail servers DNS resolvers here, as well as, or instead of, your own.
`});

export const dnsLink = (lang='en') => ({
  name: 'DNS Prefetch - Link',
  type: 'dns',
  desc: lang === 'es' ? `
En el <body> de la parte HTML, ponga una etiqueta como la siguiente:

<link rel="dns-prefetch" href="http://TRACKING_URL/">
  
Algunos clientes de correo electrónico y navegadores web realizan pre-fetching de DNS en URLs dentro de etiquets Anchor de emails. Este test no filtrará su dirección de IP, pero filtrará la dirección IP de sus resolvers DNS, que pueden proveer información sobre qué proveedor de Internet está usando y potencialmente sobre su localización.

Algunos servidores de correo hacen búsquedas de DNS en estas URL como parte de su filtrado de spam, así que puede que vea la dirección IP de los servidores resolver de DNS aquí, además de la suya.
` : `
In the <body> of the HTML part, place a tag as follows:

<link rel="dns-prefetch" href="http://TRACKING_URL/">

Some email clients and web browsers perform DNS pre-fetching on URLs in Anchor tags of emails. This test wont leak your IP address, but it will leak the IP address of your DNS resolvers, which can provide information on which ISP you're using and potentially your general location.

Some mail servers do DNS lookups on these URLs as part of their spam filtering process, so you may see the IP address of the mail servers DNS resolvers here, as well as, or instead of, your own.
`});

export const linkPrefetch = (lang='en') => ({
  name: lang === 'es' ? 'Prefetch de Enlace' : 'Link Prefetch',
  type: 'http',
  desc: lang === 'es' ? `
En el <body> de la parte HTML, ponga una etiqueta como la siguiente:

<link rel="prefetch" href="http://TRACKING_URL/">

Agentes de usuario que soporten el mecanismo de prefetch de links harán fetch a la URL de rastreo cuando se encuentren con esta etiqueta html.
` : `
In the <body> of the HTML part, place a tag as follows:

<link rel="prefetch" href="http://TRACKING_URL/">

User agents which support the link prefetch mechanism will fetch the tracking url when they come across this html tag
`});

export const flash = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta objeto - Flash' : 'Object tag - Flash',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<object width="1" height="1">
	<param name="movie" value="flash.swf"/>
	<embed src="http://TRACKING_URL/"></embed>
</object>
`});

export const fontFace = (lang='en') => ({
  name: lang === 'es' ? 'Fuente Frontal CSS' : 'CSS font-face',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<style type="text/css">@font-face {src:url('http://TRACKING_URL/');}</style>
`});

export const iframe = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta Iframe' : 'Iframe tag',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<iframe src="http://TRACKING_URL/"></iframe>
`});

export const iframeRefresh = (lang='en') => ({
  name: lang === 'es' ? 'Refrescar meta Iframe' : 'Iframe meta refresh',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<iframe src="data:text/html;charset=utf-8,&lt;html&gt;&lt;head&gt;&lt;meta http-equiv=&quot;Refresh&quot; content=&quot;1; URL=http://TRACKING_URL/&quot;&gt;&lt;/head&gt;&lt;body&gt;&lt;/body&gt;&lt;/html&gt;">
</iframe>
`});

export const iframeImg = (lang='en') => ({
  name: lang === 'es' ? 'Imagen iframe' : 'Iframe img',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<iframe src="data:text/html;charset=utf-8,&lt;html&gt;&lt;head&gt;&lt;/head&gt;&lt;body&gt;&lt;img src=&quot;http://TRACKING_URL/&quot;&gt;&lt;/body&gt;&lt;/html&gt;"></iframe>
`});

export const img = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta Imagen' : 'Image tag',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<img src="http://TRACKING_URL/">
`});

export const imgSrcset = (lang='en') => ({
  name: lang === 'es' ? 'Atributo imagen srcset' : 'Img srcset attr',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<img src="#" srcset="http://TRACKING_URL/ 1x">
`});

export const imageSubmit = (lang='en') => ({
  name: lang === 'es' ? 'Botón de Envío de Imágenes' : 'Image Submit Button',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<input type="image" src="http://TRACKING_URL/">
`});

export const js = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta de script' : 'Script tag (javascript)',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<script type="text/javascript" src="http://TRACKING_URL/"></script>
`});

export const scriptInScript = (lang='en') => ({
  name: lang === 'es' ? 'Script dentro de script' : 'Script inside script',
  type: 'http',
  desc: lang === 'es' ? `
  En el <body> de la parte HTML, ponga una etiqueta como la siguiente:

<sc<script></script>ript type="text/javascript" src="http://TRACKING_URL/"></sc<script></script>ript>

Algunas implementaciones webmail puede que quiten las dos partes "<script></script>"
` : `
In the <body> of the HTML part, place a tag as follows:

<sc<script></script>ript type="text/javascript" src="http://TRACKING_URL/"></sc<script></script>ript>

Some webmail implementations might strip out the two "<script></script>" parts
`});

export const svgBackgroundImg = (lang='en') => ({
  name: lang === 'es' ? 'Adjunto SVG con CSS' : 'SVG attachment with CSS',
  type: 'http',
  desc: lang === 'es' ? `
  En el <body> de la parte HTML, ponga una etiqueta como la siguiente:

<object data="cid:ATTACHMENT_ID" type="image/svg+xml">
	<embed src="cid:ATTACHMENT_ID" type="image/svg+xml" pluginspage="http://www.adobe.com/svg/viewer/install/" />
</object>

Después añada una multiparte/imagen relacionada/archivo svg xml al email con el encabezado MIME:

Content-ID:<ATTACHMENT_ID>

Dentro de esa imagen específicamente creada/archivo svg xml, añada una hoja de estilo CSS que realice una petición http. Específicamente:

background-image:url('http://TRACKING_URL/');
` : `
In the <body> of the HTML part, place a tag as follows:

<object data="cid:ATTACHMENT_ID" type="image/svg+xml">
	<embed src="cid:ATTACHMENT_ID" type="image/svg+xml" pluginspage="http://www.adobe.com/svg/viewer/install/" />
</object>

Then attach a multipart/related image/svg+xml file to the email with the MIME header:

Content-ID:<ATTACHMENT_ID>

Inside that specially crafted image/svg+xml file, add a CSS style which performs a http request. Specifically:

background-image:url('http://TRACKING_URL/');
`});

export const svgInlineImage = (lang='en') => ({
  name: lang === 'es' ? 'SVG en línea con imagen remota' : 'SVG inline with remote image',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<svg version="1.1" baseProfile="full" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<image xlink:href="http://TRACKING_URL/"/>
</svg>
`});

export const manifest = (lang='en') => ({
  name: 'Manifest',
  type: 'http',
  desc: (lang === 'es' ? (
    'En los atributos de la etiqueta html, ponga un atributo como el siguiente:'
  ) : (
    'In the attributes of the html tag, place an attribute as follows:'
  )) + `

<html manifest="http://TRACKING_URL/">
`});

export const metaRefresh = (lang='en') => ({
  name: lang === 'es' ? 'Meta refresco' : 'Meta refresh',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <head> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <head> of the HTML part, place a tag as follows:'
  )) + `

<meta http-equiv="Refresh" content="1; URL=http://TRACKING_URL/">
`});

export const objectData = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta objeto - datos' : 'Object tag - data',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<object data="http://TRACKING_URL/" type="image/jpeg"></object>
`});

export const opensearch = (lang='en') => ({
  name: 'OpenSearch',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <head> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <head> of the HTML part, place a tag as follows:'
  )) + `

<link rel="search" type="application/opensearchdescription+xml" href="http://TRACKING_URL/">
`});

export const returnReceipt = (lang='en') => ({
  name: lang === 'es' ? 'Recibo de vuelta' : 'Return Receipt',
  type: 'email',
  desc: lang === 'es' ? `
Añada un encabezado de mensaje como el siguiente al correo: 

Return-Receipt: <TRACKING_ADDRESS@example.com>

Puede que reciba un email alertándolo cuando el mensaje haya sido leído.
` : `
Add a message header as follows to the email message:

Return-Receipt: <TRACKING_ADDRESS@example.com>

You may then receive an email alerting you when the message has been read
`});

export const rss = (lang='en') => ({
  name: lang === 'es' ? 'Feed RSS' : 'RSS feed',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <head> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <head> of the HTML part, place a tag as follows:'
  )) + `

<link rel="alternate" type="application/rss+xml" href="http://TRACKING_URL/">
`});

export const video = (lang='en') => ({
  name: lang === 'es' ? 'Etiqueta vídeo' : 'Video tag',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<video autoplay="true" src="http://TRACKING_URL/"></video>
`});

export const videoPoster = (lang='en') => ({
  name: 'Video poster',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<video poster="http://TRACKING_URL/" autoplay="true" src="http://ANY_OTHER_URL/"></video>
`});

export const videoMp4 = (lang='en') => ({
  name: 'Video MP4',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<video autoplay="true">
  <source src="http://TRACKING_URL/" type='video/mp4; codecs="avc1.4D401E, mp4a.40.2"'/>
</video>
`});

export const videoWebm = (lang='en') => ({
  name: 'Video Webm',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<video autoplay="true">
  <source src="http://TRACKING_URL/" type='video/webm; codecs="vp8.0, vorbis"'/>
</video>
`});

export const videoOgg = (lang='en') => ({
  name: 'Video Ogg',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<video autoplay="true">
  <source src="http://TRACKING_URL/" type='video/ogg; codecs="theora, vorbis"'/>
</video>
`});

export const viewSource = (lang='en') => ({
  name: 'view-source URI',
  type: 'http',
  desc: (lang === 'es' ? (
    'En el <body> de la parte HTML, ponga una etiqueta como la siguiente:'
  ) : (
    'In the <body> of the HTML part, place a tag as follows:'
  )) + `

<iframe src="view-source:http://TRACKING_URL/"></iframe>
`});
