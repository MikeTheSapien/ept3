import mongoose from '../mongo';

import { expireTests } from '../../../config';

const Schema = new mongoose.Schema({
  code: String,
  ip:   String,
  test: String,
  http: {
    httpUserAgent:    String,
    httpForwardedFor: String,
  },
  dns: {
    ednsSubnet: String,
  },
  time: { type: Date, default: () => new Date() },
});

Schema.index(
  { time: 1 },
  { expireAfterSeconds: parseInt(expireTests / 1000, 10) }
);

export default mongoose.model('callbacks', Schema);
